import React, { Component } from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import Home from "./src/screens/Home";
import AddItem from "./src/screens/components/AddItem";
import ItemList from "./src/screens/ItemList";
import Profile from "./src/screens/Profile";

const AppNavigator = createStackNavigator(
  {
    Home,
    AddItem,
    ItemList,
    Profile
  },
  {
    initialRouteName: "Home"
  }
);
const AppContainer = createAppContainer(AppNavigator);
export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}
