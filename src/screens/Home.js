import React, { Component } from "react";
import { StyleSheet, Text, View, Image } from "react-native";

export default class HomeMenuView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.menuBox}>
          <Image
            style={styles.icon}
            source={{
              uri: "https://img.icons8.com/color/70/000000/classroom.png"
            }}
          />
          <Text
            title="ItemList"
            onPress={() => this.props.navigation.navigate("ItemList")}
            style={styles.info}
          >
            ItemList
          </Text>
        </View>
        <View style={styles.menuBox}>
          <Image
            style={styles.icon}
            source={{
              uri: "https://img.icons8.com/dusk/70/000000/visual-game-boy.png"
            }}
          />

          <Text
            title="Add an Item"
            onPress={() => this.props.navigation.navigate("AddItem")}
            style={styles.info}
          >
            Add Item
          </Text>
        </View>

        <View style={styles.menuBox}>
          <Image
            style={styles.icon}
            source={{
              uri:
                "https://img.icons8.com/color/70/000000/administrator-male.png"
            }}
          />
          <Text
            title="Profile"
            onPress={() => this.props.navigation.navigate("Profile")}
            style={styles.info}
          >
            Profile
          </Text>
          <View style={{ flex: 1 }}></View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 40,
    flexDirection: "row",
    flexWrap: "wrap"
  },
  menuBox: {
    backgroundColor: "#DCDCDC",
    width: 100,
    height: 100,
    alignItems: "center",
    justifyContent: "center",
    margin: 12
  },
  icon: {
    width: 60,
    height: 60
  },
  info: {
    fontSize: 22,
    color: "#696969"
  }
});
