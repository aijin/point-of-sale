import * as firebase from "firebase";

let config = {
  apiKey: "AIzaSyAH0vYjNk7IlslnTC7DrOVNtCEC--V_qK8",
  authDomain: "point-of-sale-8870c.firebaseapp.com",
  databaseURL: "https://point-of-sale-8870c.firebaseio.com",
  projectId: "point-of-sale-8870c",
  storageBucket: "point-of-sale-8870c.appspot.com",
  messagingSenderId: "534386518354",
  appId: "1:534386518354:web:f0e07a576321f3085d328b"
};

firebase.initializeApp(config);

var database = firebase.database();

module.exports = database;
