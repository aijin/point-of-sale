import { FlatList, View, ScrollView, Text } from "react-native";
import { Appbar, Button, TextInput } from "react-native-paper";
import React, { useState, useEffect } from "react";
import Todo from "./Todo";
import firebase from "@react-native-firebase/app";
import "@react-native-firebase/firestore";
function Todos() {
  const [pos, setPOs] = useState("");
  const ref = firebase.firestore().collection("pos");
  const [loading, setLoading] = useState(true);
  const [pos, setPos] = useState([]);

  async function addPos() {
    await ref.add({
      title: pos,
      complete: false
    });
    setPos("");
  }
  useEffect(() => {
    return ref.onSnapshot(querySnapshot => {});
  }, []);

  useEffect(() => {
    return ref.onSnapshot(querySnapshot => {
      const list = [];
      querySnapshot.forEach(doc => {
        const { title, complete } = doc.data();
        list.push({
          id: doc.id,
          title,
          complete
        });
      });

      setPos(list);

      if (loading) {
        setLoading(false);
      }
    });
  }, []);
  if (loading) {
    return null; // or a spinner
  }
  return (
    <>
      <Appbar>
        <Appbar.Content title={"TODOs List"} />
      </Appbar>
      <FlatList
        style={{ flex: 1 }}
        data={todos}
        keyExtractor={item => item.id}
        renderItem={({ item }) => <Todo {...item} />}
      />
      <TextInput label={"New Todo"} value={todo} onChangeText={setTodo} />
      <Button onPress={() => addTodo()}>Add TODO</Button>
    </>
  );
}
export default Todos;
